package br.ifsc.edu.mainactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    SQLiteDatabase bd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = findViewById(R.id.listView);

        bd = openOrCreateDatabase("meubd", MODE_PRIVATE, null);

        bd.execSQL("CREATE TABLE IF NOT EXISTS notas (" +
                    "id integer primary key autoincrement," +
                    "titulo varchar not null," +
                    "texto varchar);");

//        bd.execSQL("INSERT INTO notas (titulo, texto) VALUES ('Felicidade', 'Qualquer coisa');");

        ContentValues contentValues = new ContentValues();
        contentValues.put("titulo", "Agora estou mais feliz");
        contentValues.put("texto", "Hoje acordei feliz porque ia programar");
        bd.insert("notas", null, contentValues);

        Cursor cursor = bd.rawQuery("SELECT * FROM notas", null, null);
        cursor.moveToFirst();

        String id;
        String titulo;
        String texto;
        ArrayList<String> arrayList = new ArrayList<>();
        while (!cursor.isAfterLast()) {
//            id = cursor.getString(cursor.getColumnIndex("id"));
            titulo = cursor.getString(cursor.getColumnIndex("titulo"));
//            texto = cursor.getString(cursor.getColumnIndex("texto"));
//            Log.d("TabelaNotas", id + titulo + texto);
            arrayList.add(titulo);
            cursor.moveToNext();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                                                                android.R.layout.simple_list_item_1,
                                                                android.R.id.text1,
                                                                arrayList);

        listView.setAdapter(adapter);

    }
}
